import React, { useState } from 'react';
import { View, Image, ScrollView, Text, TextInput, Button, StyleSheet, TouchableOpacity,ImageBackground } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
//import Animated, { useSharedValue, withTiming, Easing, useAnimatedStyle, withRepeat, withSequence } from 'react-native-reanimated';
import LottieView from 'lottie-react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const SignupUser=()=>{
    const nv = useNavigation();

    const [formData,setFormData] = useState({
        email:'',
        name:'',
        password:'',
    })

    const [error,setError] = useState('');

    const handleChange = (name,value)=>{
        setFormData({...formData, [name]:value})
    }

    const handleSubmit = async () =>{
        try{
            const {data} = await axios.post(`${API_BASE_URL}/user/register`, formData);
            console.log(data)
            nv.navigate("Login"); 
        }catch(e){
            if (e.response && e.response.data) {
                setError(e.response.data.message);
            } else {
                setError('An error occurred. Please try again.');
            }
            console.log(e);
        }
    }
    return (
        <View style={[styles.container]}>
<View style={styles.header}>
                <Image style={styles.image} source={require('../assets/Logo.png')} />
                <Text style={styles.buttonText2}>Connecting Community,Empowering Business!</Text>
            </View>
            {/* 
             <View>
            <LottieView style={styles.lote} source={require('../assets/L5.json')} autoPlay loop />
            </View>*/}
           
           
          
            <View style={styles.lote2}>
            <LottieView  source={require('../assets/L2.json')} autoPlay loop />
            </View>
            
            <View style={[styles.buttonsContainer]}>
            {error ? <Text style={styles.errorText}>{error}</Text> : null}


            <TextInput placeholder="Enter your email"
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.input}
                value={formData.email}
                onChangeText={(value) => handleChange('email', value)}
            /> 
           
            <TextInput 
                style={styles.input} 
                placeholder="Username"
                value={formData.name}
                onChangeText={(value) => handleChange('name', value)}
                />

            <TextInput 
                style={styles.input} 
                placeholder="Password" 
                secureTextEntry={true} // This hides the entered text
                value={formData.password} 
                onChangeText={(value) => handleChange('password', value)}
            />

<TouchableOpacity style={styles.button} onPress={handleSubmit}>
                <Text style={styles.buttonText}>Sign Up</Text>
            </TouchableOpacity>
            </View>
           
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
    
        justifyContent: 'center',
        alignItems: 'center',
       // backgroundColor: '#1d3c45',
       backgroundColor: 'white',
       
    },
   

    header: {
        display: 'flex',
        flexDirection: 'column',
        paddingBottom:hp('0%'),
        paddingTop: hp('30%'),
       justifyContent: 'center',
       alignItems: 'center',

    },
    imagecar: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: hp('50%'),
        height: hp('50%'),
        width: wp('90%'),
        borderRadius: 10,
    },
  
    button: {
        opacity: 0.8,
        borderRadius: 15,
        paddingVertical: wp('3%'),
        backgroundColor: '#2D9D1A',
        marginVertical: hp('1%'), 
        width: wp('50%'),
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
        textAlign: 'center',
        color: 'white',
    },
    buttonsContainer: {
        alignItems: 'center',
        marginTop: hp('0%'),
        height: hp('100%'),
    },
    buttonText2: {
        fontWeight: 'bold',
        fontSize: 14,
        textTransform: 'uppercase',
        color: '#A45915',
    },
    image:{
        marginLeft:15,
        height:100,
        width:100,
    },

    lote:{
        zIndex: -1,
        height:hp('10%'),
        width: wp('10%'),
        marginBottom:hp('0%'),
   
    },
    lote2:{
        zIndex: -1,
        height:hp('40%'),
        width: wp('100%'),
   
    },
    input: {
        borderRadius: 15,
        width: wp('80%'),
        height: hp('6%'),
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 20,
        paddingLeft: 10,
      },

      errorText: {
        color: 'red',
        marginBottom: 20,
        textAlign: 'center',
    },
    
    
   

});
export default SignupUser