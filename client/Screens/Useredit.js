
import {View, TextInput, Image, StyleSheet,Text, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useNavigation } from '@react-navigation/native';

const Useredit = () => {
    const nv= useNavigation();
    return(
        <View style={[styles.maincontainer]} >
            <Image style={styles.image} source={require('../assets/sir.png')} />
            <TouchableOpacity style={styles.button} onPress={()=>nv.navigate('Home')}>
                <Text style={styles.buttonText}>Click to change Profile picture</Text>
            </TouchableOpacity>

            <Text>Enter New details</Text>
            <TextInput style={styles.input} placeholder="New Username"/>
            <TextInput style={styles.input} placeholder="New Password"/>
            <TouchableOpacity style={styles.button} onPress={()=>nv.navigate('Home')}>
                <Text style={styles.buttonText}>Submit</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Useredit;


const styles=StyleSheet.create({

    maincontainer: {
        backgroundColor:'#FFFDD0',
        height:'100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width:200,
        height:200,
        borderRadius:100,
    },
    button: {
        opacity: 0.8,
        borderRadius: 15,
        paddingVertical: wp('3%'),
        backgroundColor: '#2D9D1A',
        marginVertical: hp('1%'), 
        width: wp('50%'),
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
        textAlign: 'center',
        color: '#A45915',
    },
    input: {
        borderRadius: 15,
        width: wp('80%'),
        height: hp('6%'),
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 20,
        paddingLeft: 10,
      },
})