import React from 'react';
import { View, FlatList, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Navbar from '../Components/Navbar';

const notifications = [
    { id: '1', title: 'New Message', description: 'You have received a new message from John.' },
    { id: '2', title: 'Update Available', description: 'A new update is available for the app.' },
    { id: '3', title: 'Reminder', description: 'Don\'t forget to complete your profile.' },
    { id: '4', title: 'Promotion', description: 'Get 20% off on your next purchase.' },
    { id: '5', title: 'Friend Request', description: 'Anna has sent you a friend request.' },
];

const NotificationItem = ({ title, description }) => (
    <TouchableOpacity style={styles.notificationItem}>
        <Text style={styles.notificationTitle}>{title}</Text>
        <Text style={styles.notificationDescription}>{description}</Text>
    </TouchableOpacity>
);

const BusinessNotification = () => {
    return (
        <View style={styles.maincontainer}>
            <FlatList
                data={notifications}
                renderItem={({ item }) => <NotificationItem title={item.title} description={item.description} />}
                keyExtractor={item => item.id}
                style={styles.notificationList}
                contentContainerStyle={styles.notificationListContent}
            />
            <View style={styles.navbarContainer}>
                <Navbar />
            </View>
        </View>
    );
};

export default BusinessNotification;

const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
        marginTop:50
    },
    notificationList: {
        flex: 1,
        paddingHorizontal: 20,
    },
    notificationListContent: {
        paddingVertical: 20,
    },
    notificationItem: {
        backgroundColor: '#fff',
        padding: 15,
        marginBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ddd',
    },
    notificationTitle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    notificationDescription: {
        fontSize: 14,
        marginTop: 5,
    },
    navbarContainer: {
        height: '10%',
        justifyContent: 'flex-end',
    },
});
