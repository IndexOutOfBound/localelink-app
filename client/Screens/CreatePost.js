import React, { useState, useEffect } from 'react';
import { View, Image, TextInput, StyleSheet, Text, TouchableOpacity } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import axios from 'axios';
import { useRoute, useNavigation } from '@react-navigation/native';

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';
const CreatePost = () => {
  const nv = useNavigation();
  const route = useRoute();
  const id = route.params?.data || '';
//   const selectImagesOnMount = route.params?.selectImages || false;

  const [imageUris, setImageUris] = useState([]);
  const [captions, setCaptions] = useState('');
  const [message, setMessage] = useState('');

console.log(id.id)
  useEffect(() => {
    if (true) {
      selectImages();
    }
  }, []);

  const selectImages = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsMultipleSelection: true,
      quality: 1,
    });

    if (!result.canceled) {
      setImageUris(result.assets.map(asset => asset.uri));
    }
  };

  const uploadImages = async () => {
    if (imageUris.length === 0) return;

    const formData = new FormData();
    imageUris.forEach((uri, index) => {
      formData.append('images', {
        uri,
        type: 'image/jpeg',
        name: `photo_${index}.jpg`,
      });
    });
    formData.append('image_captions', captions);
    formData.append('businessId', id.id);
    console.log(formData);

    try {
      const response = await axios.post(`${API_BASE_URL}/posts/create`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      setMessage(response.data.message);
      setImageUris([]);
      setCaptions('');

      // Hide the message after 3 seconds
      setTimeout(() => {
        setMessage('');
        nv.navigate('BusinessHome')
      }, 2000);

    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.image2} source={require('../assets/Logo.png')} />
      </View>
      <View style={{ marginTop: 20 }}>
        
        {imageUris.length > 0 && (
          <TextInput
            placeholder="Enter captions"
            value={captions}
            onChangeText={setCaptions}
            style={styles.textInput}
          />
        )}
        <View style={styles.imageContainer}>
          {imageUris.map((uri, index) => (
            <Image key={index} source={{ uri }} style={styles.image} />
          ))}
        </View>
        {message ? <Text style={styles.successText}>{message}</Text> : null}
        <TouchableOpacity style={styles.button} onPress={uploadImages}>
          <Text style={styles.buttonText}>Upload Post</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CreatePost;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  image2: {
    marginLeft: 5,
    height: 60,
    width: 60,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 40,
    maxWidth: '95%',
  },
  button: {
    backgroundColor: '#4CAF50',
    padding: 10,
    borderRadius: 5,
    marginVertical: 10,
    alignItems: 'center',
    width: '80%',
    alignSelf: 'center',
  },
  buttonText: {
    color: '#FFF',
    fontSize: 16,
  },
  imageContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 10,
  },
  image: {
    width: 110,
    height: 100,
    margin: 5,
  },
  textInput: {
    borderColor: 'gray',
    borderWidth: 1,
    marginVertical: 10,
    padding: 10,
    borderRadius: 10,
  },
  successText: {
    color: 'green',
    marginBottom: 20,
    textAlign: 'center',
  },
});
