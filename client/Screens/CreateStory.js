import React, { useState, useEffect } from 'react';
import { View, Image, TextInput, StyleSheet, Text, TouchableOpacity } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import axios from 'axios';
import { useRoute, useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const CreateStory = () => {
  const nv=useNavigation()
  const route = useRoute();
  const _id = route.params?.data || '664830fae101b32e1f9af5e3';
//   const selectImagesOnMount = route.params?.selectImages || false;

  const [imageUris, setImageUris] = useState([]);
  const [message, setMessage] = useState('');

  useEffect(() => {
    selectImages()
  }, []);

  const selectImages = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsMultipleSelection: false,
      quality: 1,
    });

    if (!result.canceled) {
      setImageUris(result.assets.map(asset => asset.uri));
    }
  };

  const uploadImages = async () => {
    if (imageUris.length === 0) return;

    const formData = new FormData();
    imageUris.forEach((uri, index) => {
      formData.append('images', {
        uri,
        type: 'image/jpeg',
        name: `photo_${index}.jpg`,
      });
    });
    formData.append('businessId', _id);
    console.log(formData);

    try {
      const response = await axios.post(`${API_BASE_URL}/story/create`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      setMessage(response.data.message);
      setImageUris([]);

      // Hide the message after 3 seconds
      setTimeout(() => {
        setMessage('');
        nv.navigate('BusinessHome')
      }, 2000);

    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.image2} source={require('../assets/Logo.png')} />
      </View>
      <ScrollView>
      <View style={{ marginTop: 20 }}>
        
     
        <View style={styles.imageContainer}>
          {imageUris.map((uri, index) => (
            <Image key={index} source={{ uri }} style={styles.image} />
          ))}
        </View>
        {message ? <Text style={styles.successText}>{message}</Text> : null}
        <TouchableOpacity style={styles.button} onPress={uploadImages}>
          <Text style={styles.buttonText}>Upload Story</Text>
        </TouchableOpacity>
      </View>
      </ScrollView>
    </View>
  );
};

export default CreateStory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  image2: {
    marginLeft: 5,
    height: 60,
    width: 60,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 40,
    maxWidth: '95%',
  },
  button: {
    backgroundColor: '#4CAF50',
    padding: 10,
    borderRadius: 5,
    marginVertical: 10,
    alignItems: 'center',
    width: '80%',
    alignSelf: 'center',
  },
  buttonText: {
    color: '#FFF',
    fontSize: 16,
  },
  imageContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 10,
  },
  image: {
    width: 350,
    height: 500,
    
  },
  textInput: {
    borderColor: 'gray',
    borderWidth: 1,
    marginVertical: 10,
    padding: 10,
    borderRadius: 10,
  },
  successText: {
    color: 'green',
    marginBottom: 20,
    textAlign: 'center',
  },
});
