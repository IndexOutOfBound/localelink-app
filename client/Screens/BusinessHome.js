import { StyleSheet, View, ScrollView } from 'react-native';
import React, { useEffect, useState } from 'react';
import Bar from '../Components/Bar';
import Story from '../Components/Story';
import Navbar from '../Components/Navbar';
import Content from '../Components/Content';
import { useRoute } from '@react-navigation/native';
import axios from 'axios';

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const BusinessHome = () => {
    const route = useRoute();
    const _id = route.params?.data || '';
    const businessId = _id;
    const [posts, setPosts] = useState([]);
    const [filteredPosts, setFilteredPosts] = useState([]);
    const [stories, setStories] = useState([]);

    useEffect(() => {
        fetchStories();
        fetchPosts();
    }, []);

    const fetchPosts = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/posts/getposts`, {
                params: { businessId }
            });
            setPosts(response.data.posts);
            setFilteredPosts(response.data.posts); // Set filtered posts initially
        } catch (e) {
            console.log(e.message);
        }
    };

    const fetchStories = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/story/getstory`);
            setStories(response.data.storys);
        } catch (e) {
            console.log(e.message);
        }
    };

    const handleSearch = (query) => {
        const filtered = posts.filter(post => post.business.businessname.toLowerCase().includes(query.toLowerCase()));
        setFilteredPosts(filtered);
    };

    return (
        <View style={styles.mainContainer}>
            <View>
                <Bar onSearch={handleSearch} />
            </View>
            <View>
                <Story stories={stories} />
            </View>
            <ScrollView vertical style={styles.scrollView}>
                {filteredPosts.map((post, index) => (
                    <Content key={index} post={post} entityId={businessId} entityType='business' />
                ))}
            </ScrollView>
            <Navbar id={businessId}/>
        </View>
    );
};

export default BusinessHome;

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: 'white',
        height: '100%',
    },
    scrollView: {
        flex: 1,
    },
});
