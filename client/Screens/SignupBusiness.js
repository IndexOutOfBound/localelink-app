import React, { useState } from 'react';
import { View, Image, ScrollView, Text, TextInput, Button, Modal, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';


// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1'
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';
const SignupBusiness = () => {
    const navigation = useNavigation();

    const [formData, setFormData] = useState({
        email: '',
        name: '',
        phonenumber: '',
        location: '',
        category: '',
        aboutbusiness: '',
        password: '',
    });

    const handleChange = (name, value) => {
        setFormData({ ...formData, [name]: value });
    };
    const [error,setError] = useState('');

    const handleSubmit = async () => {
        try {
            const { data } = await axios.post(`${API_BASE_URL}/bussines/addusers`, formData);
            console.log(data);
            navigation.navigate("Login"); // Updated navigation route
        } catch (e) {
            if (e.response && e.response.data) {
                setError(e.response.data.message);
            } else {
                setError('An error occurred. Please try again.');
            }
            console.log(e);
            console.log(e);
        }
    };

    return (
        <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.header}>
                <Image style={styles.image} source={require('../assets/Logo.png')} />
                <Text style={styles.buttonText2}>Connecting Community, Empowering Business!</Text>
            </View>

            <View style={styles.lote2}>
                <LottieView source={require('../assets/L2.json')} autoPlay loop />
            </View>

            <View style={styles.buttonsContainer}>
                {error ? <Text style={styles.errorText}>{error}</Text> : null}
                <TextInput
                    placeholder="Email"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={formData.email}
                    style={styles.input}
                    onChangeText={(value) => handleChange('email', value)}
                />

                <TextInput
                    style={styles.input}
                    placeholder="Name"
                    value={formData.name}
                    onChangeText={(value) => handleChange('name', value)}
                />

                <TextInput
                    placeholder="Phone Number"
                    keyboardType="phone-pad"
                    style={styles.input}
                    value={formData.phonenumber}
                    onChangeText={(value) => handleChange('phonenumber', value)}
                />

                <TextInput
                    style={styles.input}
                    placeholder="Location"
                    value={formData.location}
                    onChangeText={(value) => handleChange('location', value)}
                />

                <TextInput
                    style={styles.input}
                    placeholder="Category"
                    value={formData.category}
                    onChangeText={(value) => handleChange('category', value)}
                />

                <TextInput
                    multiline={true}
                    numberOfLines={10}
                    placeholder="About Business"
                    style={styles.input2}
                    value={formData.aboutbusiness}
                    onChangeText={(value) => handleChange('aboutbusiness', value)}
                />

                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    secureTextEntry={true}
                    value={formData.password}
                    onChangeText={(value) => handleChange('password', value)}
                />

                <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                    <Text style={styles.buttonText}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    header: {
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: hp('0%'),
        paddingTop: hp('10%'),
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        opacity: 0.8,
        borderRadius: 15,
        paddingVertical: wp('3%'),
        backgroundColor: '#2D9D1A',
        marginVertical: hp('1%'),
        width: wp('50%'),
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
        textAlign: 'center',
        color: 'white',
    },
    buttonsContainer: {
        alignItems: 'center',
        marginTop: hp('0%'),
        height: hp('100%'),
    },
    buttonText2: {
        fontWeight: 'bold',
        fontSize: 14,
        textTransform: 'uppercase',
        color: '#A45915',
    },
    image: {
        marginLeft: 15,
        height: 100,
        width: 100,
    },
    lote2: {
        zIndex: -1,
        height: hp('40%'),
        width: wp('100%'),
    },
    input: {
        borderRadius: 15,
        width: wp('80%'),
        height: hp('6%'),
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 20,
        paddingLeft: 10,
    },
    input2: {
        borderRadius: 15,
        width: wp('80%'),
        height: hp('10%'),
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 20,
        paddingLeft: 10,
    },

    errorText: {
        color: 'red',
        marginBottom: 20,
        textAlign: 'center',
    },
});

export default SignupBusiness;
