// UserProfile.js

import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Button, Image } from 'react-native';
import axios from 'axios';
import { useRoute, useNavigation } from '@react-navigation/native';
// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';
const UserProfile = () => {
  const [userData, setUserData] = useState(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const route = useRoute();
  const nv= useNavigation();
  const {data}   = route.params?.data || '';
  const userId= data;
  console.log(userId);
  if(userId===null){
    nv.navigate('Login')
  }
  useEffect(() => {
    // Fetch user data when component mounts
    const fetchUserData = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/bussines/getuserdata/${userId}`);
        setUserData(response.data);
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };
    fetchUserData();
  }, []);

  const handleUpdateProfile = async () => {
    try {
      await axios.put(`${API_BASE_URL}/bussines/updateuserdata/${userId}`, { name, email });
      // Refresh user data after updating
      const response = await axios.get(`${API_BASE_URL}/bussines/getuserdata/${userId}`);
      setUserData(response.data);
    } catch (error) {
      console.error('Error updating profile:', error);
    }
  };

  const handleLogout = (req,res)=>{
    nv.navigate('Login')
  }
  return (
    <View style={styles.container}>
      {userData ? (
        <>
          <View style={styles.profileContainer}>
            <Image source={require('../assets/sir.png')} style={styles.profileImage} />
            <View style={styles.infoContainer}>
              <Text style={styles.businessName}>{userData.name}</Text>
              <Text style={styles.businessCategory}>{userData.email}</Text>
            </View>
          </View>
          <TextInput
            style={styles.input}
            value={name}
            onChangeText={setName}
            placeholder="Enter your name"
          />
          <TextInput
            style={styles.input}
            value={email}
            onChangeText={setEmail}
            placeholder="Enter your email"
          />
       <View style={styles.buttonContainer}>
        <View style={styles.button}>
            <Button title="Update Profile" onPress={handleUpdateProfile}  />
          </View>

            <View style={styles.button}>
              <Button title="Logout" onPress={handleLogout}/>
            </View>
       </View>
        </>
      ) : (
        <Text>Loading...</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button:{
    margin:10,
  },
  buttonContainer:{
    display: 'flex',
    flexDirection: 'row',
  },
  profileContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 20,
  },
  infoContainer: {
    justifyContent: 'center',
  },
  businessName: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  businessCategory: {
    fontSize: 16,
    color: 'gray',
  },
  input: {
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
    width: 300,
  },
});

export default UserProfile;
