import React, { useState } from 'react';
import { View, Image, ScrollView, Text, TextInput, Button, StyleSheet, TouchableOpacity,ImageBackground } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
//import Animated, { useSharedValue, withTiming, Easing, useAnimatedStyle, withRepeat, withSequence } from 'react-native-reanimated';
import LottieView from 'lottie-react-native';
import { useNavigation } from '@react-navigation/native';


const RegisterPage = () => {
    const nv = useNavigation();
    return (
        <View style={[styles.container]}>
<View style={styles.header}>
                <Image style={styles.image} source={require('../assets/Logo.png')} />
                <Text style={styles.buttonText2}>Connecting Community,Empowering Business!</Text>
            </View>
            {/* 
             <View>
            <LottieView style={styles.lote} source={require('../assets/L5.json')} autoPlay loop />
            </View>*/}
           
           
          
            <View style={styles.lote2}>
            <LottieView  source={require('../assets/L2.json')} autoPlay loop />
            </View>
            
            <View style={[styles.buttonsContainer]}>
           
            <TouchableOpacity style={styles.button} onPress={()=>nv.navigate('Login')} >
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button} onPress={()=>nv.navigate('UserRegister')}>
                <Text style={styles.buttonText}>Register</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button} onPress={()=>nv.navigate('BusinessRegister')} >
                <Text style={styles.buttonText}>Register as a Business</Text>
            </TouchableOpacity>
            </View>
           
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
    
        justifyContent: 'center',
        alignItems: 'center',
       // backgroundColor: '#1d3c45',
    //    backgroundColor: '#FFFDD0',
        backgroundColor: '#FFFFFF',
    },
   

    header: {
        display: 'flex',
        flexDirection: 'column',
        paddingBottom:hp('0%'),
        paddingTop: hp('30%'),
       justifyContent: 'center',
       alignItems: 'center',

    },
    imagecar: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: hp('50%'),
        height: hp('50%'),
        width: wp('90%'),
        borderRadius: 10,
    },
  
    button: {
        opacity: 0.8,
        borderRadius: 15,
        paddingVertical: wp('3%'),
        backgroundColor: '#2D9D1A',
        marginVertical: hp('1%'), 
        width: wp('85%'),
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
        textAlign: 'center',
        color: 'white',
    },
    buttonsContainer: {
        alignItems: 'center',
        marginTop: hp('0%'),
        height: hp('100%'),
    },
    buttonText2: {
        fontWeight: 'bold',
        fontSize: 14,
        textTransform: 'uppercase',
        color: '#A45915',
    },
    image:{
        marginLeft:15,
        height:100,
        width:100,
    },

    lote:{
        zIndex: -1,
        height:hp('10%'),
        width: wp('10%'),
        marginBottom:hp('0%'),
   
    },
    lote2:{
        zIndex: -1,
        height:hp('40%'),
        width: wp('100%'),
   
    }
    
    
   

});
export default RegisterPage;
