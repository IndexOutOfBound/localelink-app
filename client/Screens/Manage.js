import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import MenuSer from '../Components/MenuSer';
import Rating from '../Components/Rating';

const Manage = () => {
  const [activeComponent, setActiveComponent] = useState('MenuSer');

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          style={[styles.headerButton, activeComponent === 'MenuSer' && styles.activeButton]}
          onPress={() => setActiveComponent('MenuSer')}
        >
          <Text style={styles.headerText}>Menu/Service</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.headerButton, activeComponent === 'Rating' && styles.activeButton]}
          onPress={() => setActiveComponent('Rating')}
        >
          <Text style={styles.headerText}>Rating</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.content}>
        {activeComponent === 'MenuSer' ? <MenuSer /> : <Rating />}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#f8f8f8',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    marginTop:50,
  },
  headerButton: {
    padding: 10,
  },
  headerText: {
    fontSize: 18,
  },
  activeButton: {
    borderBottomWidth: 2,
    borderBottomColor: '#007BFF',
  },
  content: {
    flex: 1,
  },
});

export default Manage;
