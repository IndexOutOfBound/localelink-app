import { StyleSheet, Text, View,Image, TextInput } from 'react-native'
import React,{useState,useEffect} from 'react'
import Bar from '../Components/Bar'
import Story from '../Components/Story'
import { ScrollView } from 'react-native-gesture-handler'
import Content from '../Components/Content'
import Userbot from '../Components/Userbot'
import { useRoute } from '@react-navigation/native';
import axios from 'axios';
import UserStory from '../Components/UserStory'

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';
const HomePage = () => {
    const route = useRoute()
    const _id = route.params?.data || '';
    const userId= _id;

    const [posts, setPosts] = useState([]);
    const [filteredPosts, setFilteredPosts] = useState([]);
    useEffect(() => {
        fetchstory();
        fetchPosts();
        
    }, []);

    const fetchPosts = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/posts/getposts`,{
                params: { userId }
            });
            // console.log(response.data.posts);
            setPosts(response.data.posts);
            setFilteredPosts(response.data.posts);
        } catch (e) {
            console.log(e.message);
        }
    };
    
    
    const [story, setStory] = useState([]);
  
 

    const fetchstory = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/story/getstory`);
            setStory(response.data.storys);
        } catch (e) {
            console.log(e.message);
        }
    };
    const handleSearch = (query) => {
        const filtered = posts.filter(post => post.business.businessname.toLowerCase().includes(query.toLowerCase()));
        setFilteredPosts(filtered);
    };
  return (
    <View style={styles.mainContainer}>
      
<View>
    <Bar onSearch={handleSearch}/>
</View>

<View>
    <UserStory stories={story}/>
</View>


<ScrollView vertical style={styles.scrollView}>
                {filteredPosts.map((post, index) => (
                    <Content key={index} post={post} entityId={userId} entityType='user' />
                ))}
            </ScrollView>

<Userbot data={userId}/>
    </View>
  )
}

export default HomePage;

const styles = StyleSheet.create({
  
    mainContainer:{
        backgroundColor:'white',
        height:"100%",
    },
    scrollView: {
        flex: 1,
      },

 

 
 
})