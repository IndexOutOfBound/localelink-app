import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TextInput, Button, TouchableOpacity } from 'react-native';
import { useRoute } from '@react-navigation/native';
import axios from 'axios';
import StarRating from '../Components/StarRating';

const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const ServiceView = () => {
    const route = useRoute();
    const { _id  } = route.params?.data || '';
    const businessId = _id;
    const {entityId, entityType} = route.params;
    
    const [business, setBusiness] = useState(null);
    const [services, setServices] = useState([]);
    const [ratings, setRatings] = useState([]);
    const [ratingText, setRatingText] = useState('');
    const [ratingScore, setRatingScore] = useState('');
    const [selectedTab, setSelectedTab] = useState('serviceMenu');

    useEffect(() => {
        const fetchBusinessDetails = async () => {
            try {
                const response = await axios.get(`${API_BASE_URL}/service/getdetailsforuser/${businessId}`);
                setBusiness(response.data.business);
                setServices(response.data.services);
            } catch (error) {
                console.error('Error fetching business details:', error);
            }
        };

        fetchBusinessDetails();
    }, [businessId]);

    const fetchRatings = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/rating/getrating/${businessId}`);
            setRatings(response.data.ratings);
        } catch (error) {
            console.error('Error fetching ratings:', error);
        }
    };

    useEffect(() => {
        fetchRatings();
    }, [businessId]);

    const submitRating = async () => {
        
        if (ratingText && ratingScore) {
            try {
                const datatosend ={
                    business_id: businessId,
                    ratingText: ratingText,
                    ratingScore: ratingScore,
                }
                if (entityType === 'business') {
                    datatosend.business= entityId;
                    datatosend.user = null;
                } else {
                    datatosend.user = entityId;
                    datatosend.business = null;
                }
                await axios.post(`${API_BASE_URL}/rating/addrating`, datatosend);
                setRatingText('');
                setRatingScore('');
                fetchRatings();
            } catch (error) {
                console.error('Error submitting rating:', error);
            }
        }
    };

    const averageRating = () => {
        if (ratings.length === 0) return 0;
        const totalRating = ratings.reduce((acc, rating) => acc + parseInt(rating.ratingScore), 0);
        return totalRating / ratings.length;
    };

    const renderItem = ({ item }) => (
        <View style={styles.row}>
            <Text style={styles.serviceName}>{item.service}</Text>
            <Text style={styles.servicePrice}>Nu. {item.price}</Text>
        </View>
    );

    const renderRating = ({ item }) => (
        <View style={styles.ratingRow}>
            <Text style={styles.ratingText}>{item.content}</Text>
            <StarRating rating={parseInt(item.ratingScore)} />
        </View>
    );

    if (!business) {
        return <Text>Loading...</Text>;
    }

    return (
        <View style={styles.container}>
            <View style={styles.profileContainer}>
                <View style={styles.inner}>
                    <Image
                        source={require('../assets/sir.png')}
                        style={styles.profileImage}
                    />
                </View>
                <View style={styles.infoContainer}>
                    <Text style={styles.businessName}>{business.businessname}</Text>
                    <Text style={styles.businessCategory}>Category: {business.category}</Text>
                    <Text style={styles.businessLocation}>Location: {business.location}</Text>
                    <Text style={styles.businessContact}>Contact: {business.phonenumber}</Text>
                    <View>
                        <StarRating rating={averageRating()} />
                    </View>
                </View>
            </View>
            
            <View style={styles.tabContainer}>
                <TouchableOpacity 
                    style={[styles.tabButton, selectedTab === 'serviceMenu' && styles.activeTabButton]} 
                    onPress={() => setSelectedTab('serviceMenu')}
                >
                    <Text style={[styles.tabText, selectedTab === 'serviceMenu' && styles.activeTabText]}>Service/Menu</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={[styles.tabButton, selectedTab === 'ratings' && styles.activeTabButton]} 
                    onPress={() => setSelectedTab('ratings')}
                >
                    <Text style={[styles.tabText, selectedTab === 'ratings' && styles.activeTabText]}>Rating/Review</Text>
                </TouchableOpacity>
            </View>
            
            {selectedTab === 'serviceMenu' ? (
                <>
                    {services.length === 0 ? (
                        <Text style={styles.noServicesText}>This business hasn't posted anything yet.</Text>
                    ) : (
                        <FlatList
                            data={services}
                            renderItem={renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    )}
                </>
            ) : (
                <>
                    {ratings.length === 0 ? (
                        <Text style={styles.noRatingsText}>No ratings yet.</Text>
                    ) : (
                        <FlatList
                            data={ratings}
                            renderItem={renderRating}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    )}
                    <View style={styles.ratingForm}>
                        <TextInput
                            style={styles.ratingInput}
                            placeholder="Rating Text"
                            value={ratingText}
                            onChangeText={setRatingText}
                        />
                        <TextInput
                            style={styles.ratingInput}
                            placeholder="Rating Score (1-5)"
                            value={ratingScore}
                            onChangeText={setRatingScore}
                            keyboardType="numeric"
                        />
                        <Button title="Submit" onPress={submitRating} />
                    </View>
                </>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#fff',
    },
    profileContainer: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    inner: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginRight: 20,
    },
    infoContainer: {
        justifyContent: 'center',
        marginTop: 70
    },
    businessName: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    businessCategory: {
        fontSize: 16,
        color: 'gray',
    },
    businessLocation: {
        fontSize: 16,
        color: 'gray',
    },
    businessContact: {
        fontSize: 16,
        color: 'gray',
    },
    tabContainer: {
        flexDirection: 'row',
        marginBottom: 20,
        borderRadius: 20,
    },
    tabButton: {
        flex: 1,
        paddingVertical: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        alignItems: 'center',
    },
    activeTabButton: {
        backgroundColor: '#2D9D1A',
        borderColor: '#2D9D1A',
    },
    tabText: {
        fontSize: 16,
        color: '#2D9D1A',
    },
    activeTabText: {
        color: '#fff',
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 20,
        textAlign: 'center',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    serviceName: {
        fontSize: 16,
    },
    servicePrice: {
        fontSize: 18,
        color: '#888',
    },
    noServicesText: {
        fontSize: 16,
        color: 'gray',
        textAlign: 'center',
        marginTop: 20,
    },
    ratingRow: {
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    ratingScore: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    ratingText: {
        fontSize: 16,
        color: '#888',
    },
    noRatingsText: {
        fontSize: 16,
        color: 'gray',
        textAlign: 'center',
        marginTop: 20,
    },
    ratingForm: {
        marginTop: 20,
    },
    ratingFormTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center',
    },
    ratingInput: {
        borderWidth: 1,
        borderColor: '#ccc',
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
});

export default ServiceView;
