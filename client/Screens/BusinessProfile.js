import React, { useState, useEffect } from 'react';
import { View, Image, StyleSheet, Text, FlatList, ScrollView, TouchableOpacity, Switch } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import Navbar from '../Components/Navbar';
import axios from 'axios';

const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const BusinessProfile = () => {
  const route = useRoute();
  const _id = route.params?.data || '';
  console.log(_id.id)
  const businessId = _id.id;
  const nv = useNavigation();
  const [isOpen, setIsOpen] = useState(false);
  const [photos, setPhotos] = useState([]);
  const [userdata,setUserData]= useState([]);
  if(businessId ===null){
    nv.navigate('Login')
  }

  useEffect(() => {
    const fetchPhotos = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/posts/businessposts/${businessId}`);
        setPhotos(response.data);
      } catch (error) {
        console.error('Error fetching photos:', error);
      }
    };

    fetchPhotos();
    getbusinessdata();
  }, [businessId]);

  const handlePress = () => {
    navigation.navigate('Manage', { data: _id });
  };

  const getbusinessdata = async() => {
    try{
      const response = await axios.get(`${API_BASE_URL}/bussines/getbusinessdata/${businessId}`)
      setUserData(response.data);
      console.log(response.data);
    }catch(e){
      console.log(e);
    }
  }
  const handleLogout = ()=>{
    nv.navigate('Login');
  }
 
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.profileContainer}>
          <View style={styles.inner}>
            <Image source={require('../assets/sir.png')} style={styles.profileImage} />
          </View>
          <View style={styles.infoContainer}>
            <Text style={styles.businessName}>{userdata.businessname}</Text>
            <Text style={styles.businessCategory}>{userdata.category}</Text>
            <Text style={styles.businessLocation}>{userdata.email}</Text>
            <Text style={styles.businessLocation}>{userdata.phonenumber}</Text>
            {/* <View style={styles.switchContainer}>
              <Text style={styles.switchLabel}>{isOpen ? 'Open' : 'Closed'}</Text>
              <Switch
                value={isOpen}
                onValueChange={(value) => setIsOpen(value)}
                trackColor={{ false: '#767577', true: '#2D9D1A' }}
                thumbColor={isOpen ? '#2D9D1A' : '#f4f3f4'}
              />
            </View> */}
            <TouchableOpacity style={styles.button} onPress={handlePress}>
              <Text style={styles.buttonText}>Manage Business</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={handleLogout}>
              <Text style={styles.buttonText}>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.inner2}>
          <View style={{ marginLeft: 0, marginRight: 5 }}>
            <Text style={styles.photosTitle}>Uploads</Text>
          </View>
        </View>

        <View style={{ marginBottom: 70 }}>
          <FlatList
            data={photos}
            keyExtractor={(item) => item.postId}
            renderItem={({ item }) => (
              <Image source={{ uri: `http://10.9.84.110:8080/${item.image}` }} style={styles.photo} />
            )}
            numColumns={3}
          />
        </View>
      </ScrollView>


        <Navbar />
  
    </View>
  );
};

export default BusinessProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  contentContainer: {
    padding: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  profileContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    marginTop: 50,
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 20,
  },
  infoContainer: {
    flex: 1,
  },
  businessName: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  businessCategory: {
    fontSize: 16,
    color: 'gray',
  },
  businessLocation: {
    fontSize: 16,
    color: 'gray',
  },
  switchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  switchLabel: {
    fontSize: 16,
    marginRight: 10,
  },
  button: {
    backgroundColor: '#2D9D1A',
    padding: 5,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  buttonText: {
    color: '#FFF',
    fontSize: 15,
  },
  inner: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  inner2: {
    flexDirection: 'row',
    marginLeft: 0,
    width: '100%',
  },
  photosTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    alignSelf: 'flex-start',
  },
  photo: {
    width: 120,
    height: 120,
    margin: 2,
    borderWidth: 1,
    borderColor: 'black',
  },
  navbarContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
});
