import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, Share, Dimensions, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import PagerView from 'react-native-pager-view';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const { width: screenWidth } = Dimensions.get('window');

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const Content = ({ post, entityId, entityType }) => {
    const [isLiked, setIsLiked] = useState(post.isLiked);
    const [likeCount, setLikeCount] = useState(post.likeCount);
    const [showComments, setShowComments] = useState(false);
    const [numCommentsToShow, setNumCommentsToShow] = useState(5);
    const [commentContent, setCommentContent] = useState('');
    const [comments, setComments] = useState([])
    const nv = useNavigation();

    const toggleLike = async () => {
        try {
            const likeData = { postId: post._id };
            if (entityType === 'business') {
                likeData.businessId = entityId;
                likeData.userId = null;
            } else {
                likeData.userId = entityId;
                likeData.businessId = null;
            }
            console.log(likeData);
            if (isLiked) {
                await axios.post(`${API_BASE_URL}/like/unlike`, likeData);
                setLikeCount(likeCount - 1);
            } else {
                await axios.post(`${API_BASE_URL}/like/like`, likeData);
                setLikeCount(likeCount + 1);
            }
            setIsLiked(!isLiked);
        } catch (error) {
            console.error('Error toggling like:', error.message);
        }
    };

    const toggleComments = () => {
        setShowComments(!showComments);
    };

    // const comments = [
    //     { text: "This restaurant is awesomeeee sure awesome", profileImage: require('../assets/sir.png') },
    //     // Add more comments as needed
    // ];

    const shareContent = async () => {
        try {
            const result = await Share.share({
                message: 'Locale Link is the best App to Explore',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // Shared via activity type
                } else {
                    // Shared
                }
            } else if (result.action === Share.dismissedAction) {
                // Dismissed
            }
        } catch (error) {
            console.error('Error sharing:', error.message);
        }
    };

    useEffect(() => {
        if (!showComments) {
            setNumCommentsToShow(5);
        }
        getComment();
    }, [showComments]);

    const handleShowMoreComments = () => {
        setNumCommentsToShow(prevNum => prevNum + 5);
    };

    const handleAddComment = async () => {
        try {
            const commentData = {
                content: commentContent,
            };
            if (entityType === 'business') {
                commentData.business_id = entityId;
                commentData.user_id = null;
            } else {
                commentData.user_id = entityId;
                commentData.business_id = null;
            }
            const post_id = post._id
            console.log('post_id',post_id);
            console.log(commentData);
            await axios.post(`${API_BASE_URL}/comment/create/${post_id}`, commentData);
            setCommentContent('');
            // Optionally, update comments state to reflect new comment
        } catch (error) {
            console.error('Error adding comment:', error.message);
        }
    };

    const getComment = async() =>{
        try{
            const post_id = post._id;
            const {data} = await axios.get(`${API_BASE_URL}/comment/getallcomments`,{
                params : {post_id}
            })
            setComments(data)
            console.log(comments)
        }catch(error){
            console.log(error.message)
        }
    }
    

    return (
        <View style={styles.feed}>
            <View style={styles.header}>
                <View style={styles.headerLeft}>
                <TouchableOpacity onPress={() => nv.navigate('ServiceView', { data: post.business, entityId: entityId, entityType: entityType })}>

                        <Image
                            source={require('../assets/sir.png')}
                            style={styles.profileImage}
                        />
                    </TouchableOpacity>
                    <Text style={styles.businessName}>{post.business?.businessname || post.user?.username}</Text>
                </View>
                <Icon style={styles.icon} name="ellipsis-vertical" size={20} color="black" />
            </View>
            
            <View style={styles.imageContainer}>
                <PagerView style={styles.pagerView} initialPage={0}>
                    {post.image_paths.map((imageUri, index) => (
                        <View key={index} style={styles.imageWrapper}>
                            <Image style={styles.image} source={{ uri: `http://10.9.84.110:8080/${imageUri}` }} />
                        </View>
                    ))}
                </PagerView>
            </View>
            <View style={styles.interactions}>
                <Text style={styles.likesComments}>{likeCount} Likes, 0 comments</Text>
                <View style={styles.icons}>
                    <TouchableHighlight onPress={toggleLike} underlayColor="transparent">
                        <Icon name={isLiked ? "heart" : "heart-outline"} size={36} color={isLiked ? "red" : "#A45915"} style={styles.heartIcon} />
                    </TouchableHighlight>
                    <TouchableHighlight onPress={toggleComments} underlayColor="transparent">
                        <Icon name="chatbubble-outline" size={32} color="#A45915" style={styles.chatIcon} />
                    </TouchableHighlight>
                    <Icon name="share-social-outline" size={32} color="#A45915" onPress={shareContent} style={styles.shareIcon} />
                </View>
            </View>
            <View style={styles.captionContainer}>
                <Text style={styles.caption}>
                    <Text style={styles.captionUsername}>{post.business?.businessname || post.user?.username}</Text> {post.image_captions}
                </Text>
            </View>
            {showComments && (
                <View style={styles.commentSection}>
                    {/* <Text>Comments</Text> */}
                    {comments.slice(0, numCommentsToShow).map((comment, index) => (
                    <View key={index} style={styles.commentContainer}>
                        {comment.business ? (
                        <Image source={require('../assets/sir.png')} style={styles.profileImage} />
                        ) : comment.user ? (
                        <View style={[styles.profileImage, styles.colorBlock]} />
                        ) : null}
                        <Text style={styles.commentText}>{comment.content}</Text>
                    </View>
                    ))}
                    
                    {comments.length > numCommentsToShow && (
                        <TouchableHighlight onPress={handleShowMoreComments} underlayColor="transparent">
                            <Text style={styles.showMore}>Show more</Text>
                        </TouchableHighlight>
                    )}
                    <View style={{ display: 'flex', flexDirection: 'row' }}>
                        <TextInput 
                            placeholder='Add a comment...' 
                            style={styles.input} 
                            value={commentContent}
                            onChangeText={setCommentContent}
                        />
                        <TouchableOpacity onPress={handleAddComment}>
                            <Icon name='send' size={28} style={{ margin: 5 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    feed: {
        width: '100%',
        justifyContent: 'space-around',
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding:6,
    },
    headerLeft: {
        flexDirection: 'row',
    },
    icon: {
        marginTop: 14,
        color: "#2D9D1A",
        marginLeft: 5,
    },
    businessName: {
        marginTop: 14,
        marginLeft: 5,
        fontWeight: 'bold',
        fontSize: 14,
    },
    captionContainer: {
        marginHorizontal: 8,
        marginBottom: 30,
    },
    caption: {
        fontSize: 14,
        lineHeight: 18,
        color: '#A45915',
    },
    captionUsername: {
        fontWeight: 'bold',
    },
    imageContainer: {
        height: 400, // Adjust this value as needed
        
    },
    pagerView: {
        flex: 1,
    },
    imageWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: screenWidth,
        height: '100%',
        resizeMode: 'cover',
    },
    interactions: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 5,
        marginRight: 10,
        
    },
    likesComments: {
        marginTop: 20,
        marginLeft: 3,
        fontSize: 12,
        color: "#A45915",
    },
    icons: {
        flexDirection: 'row',
        marginRight: 5,
        marginBottom: 10,
    },
    chatIcon: {
        marginLeft: 25,
        marginTop: 5
    },
    shareIcon: {
        marginLeft: 25,
        marginTop: 5
    },
    heartIcon: {
        marginLeft: 25,
        marginTop: 3,
    },
    commentSection: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 3,
        borderColor: "white",
    },
    profileImage: {
        height: 30,
        width: 30,
        borderRadius: 25,
        borderColor: '#2D9D1A',
        borderWidth: 2,
        margin: 5,
    },
    commentContainer: {
        // borderWidth: 1,
        margin: 5,
        borderRadius: 20,
        width: "100%",
        display:'flex',
        flexDirection: 'row'
    },
    commentText: {
        margin: 10,
    },
    showMore: {
        color: '#A45915',
        marginTop: 5,
        marginBottom: 5,
    },
    input: {
        borderRadius: 15,
        width: wp('80%'),
        height: hp('5%'),
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 20,
        paddingLeft: 10,
      },
      colorBlock: {
        backgroundColor: '#ccc', // Adjust this color as needed
      },
});

export default Content;
