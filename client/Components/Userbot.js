import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
const Userbot = (data) => {
    const nv = useNavigation();
  return (
    <View style={styles.container}>
      <Icon name="home" size={30} color="#2D9D1A" onPress={()=>nv.navigate('Home')}/>
      <Icon name="notifications-sharp" size={30} color="#2D9D1A" onPress={()=>nv.navigate('UserNotification')} />
      <Icon name="heart" size={34} color="#2D9D1A" />
      <Icon name="person" size={30} color="#2D9D1A" onPress={()=>nv.navigate('UserProfile', {data:data})}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 1,
    borderTopColor: 'gray',
    position: 'fixed',
    bottom: 0,
    
  },
});

export default Userbot;
