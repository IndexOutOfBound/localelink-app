import React from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const StarRating = ({ rating }) => {
    const filledStars = Math.floor(rating);
    const hasHalfStar = rating - filledStars >= 0.5;
    const emptyStars = 5 - filledStars - (hasHalfStar ? 1 : 0);

    return (
        <View style={{ flexDirection: 'row' }}>
            {[...Array(filledStars)].map((_, index) => (
                <Icon name="star" key={index} size={15} color="#FFD700" />
            ))}
            {hasHalfStar && <Icon name="star-half" size={15} color="#FFD700" />}
            {[...Array(emptyStars)].map((_, index) => (
                <Icon name="star-outline" key={index} size={15} color="#FFD700" />
            ))}
        </View>
    );
};

export default StarRating;
