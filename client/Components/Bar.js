import { StyleSheet, View, Image, TextInput, Platform } from 'react-native';
import React,{useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons'; // Import Icon from react-native-vector-icons

const Bar = ({ onSearch }) => {
    const [searchQuery, setSearchQuery] = useState('');

    const handleSearch = () => {
        onSearch(searchQuery);
    };

    return (
        <View style={styles.mainContainer}>
            <View style={styles.header}>
                <View>
                    <Image style={styles.image} source={require('../assets/Logo.png')} />
                </View>

                <View style={styles.search}>
                    <TextInput
                        style={styles.inputField}
                        value={searchQuery}
                        onChangeText={setSearchQuery}
                        placeholder="Search by business name"
                    />
                    <Icon
                        style={{ marginTop: 14, color: "#2D9D1A" }}
                        name="search"
                        size={30}
                        color="black"
                        onPress={handleSearch}
                    />
                </View>
            </View>
        </View>
    );
};

export default Bar;

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: 'white',
    },
    image: {
        marginLeft: 15,
        height: 60,
        width: 60,
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 50,
        justifyContent: "space-between",
        maxWidth: "95%",
    },
    search: {
        display: 'flex',
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
                shadowRadius: 4,
            },
            android: {
                elevation: 5,
            },
        }),
    },
    inputField: {
        borderColor: '#2D9D1A',
        borderWidth: 1,
        width: 200,
        height: 30,
        marginTop: 15,
        borderRadius: 10,
        fontSize: 10,
        paddingHorizontal: 10, // Add padding for better appearance
        ...Platform.select({
            ios: {
                backgroundColor: 'white', // Add a background color for iOS to enable shadow
            },
            android: {
                backgroundColor: 'transparent', // Make background transparent for Android
            },
        }),
    },
});

