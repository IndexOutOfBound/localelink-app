import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';

const Navbar = (id) => {
    const nv = useNavigation();
    const handleIconPress = () => {
      nv.navigate('CreatePost', {data: id });
    };
  
  return (
    <View style={styles.container}>
      <Icon name="home" size={30} color="#2D9D1A" onPress={()=>nv.navigate('BusinessHome')}/>
      <Icon name="notifications-sharp" size={30} color="#2D9D1A" onPress={()=>nv.navigate('BusinessNotification')} />
      <Icon name="add-circle" size={30} color="#2D9D1A" onPress={handleIconPress} />
      <Icon name="person" size={30} color="#2D9D1A" onPress={()=>nv.navigate('BusinessProfile', {data: id})}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 1,
    borderTopColor: 'gray',
    position: 'fixed',
    bottom: 0,
    
  },
});

export default Navbar;
