import React, { useEffect, useState } from 'react';
import { View, TextInput, Text, TouchableOpacity, StyleSheet, Alert, ScrollView } from 'react-native';
import { useRoute } from '@react-navigation/native';
import axios from 'axios';

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';

const MenuSer = () => {
  const route = useRoute();
  const id = route.params?.data || '';
  const [services, setServices] = useState([{ serviceName: '', price: '' }]);
  const [data, setData] = useState([]);

  const handleAddService = () => {
    setServices([...services, { serviceName: '', price: '' }]);
  };

  const handleServiceNameChange = (text, index) => {
    const updatedServices = [...services];
    updatedServices[index].serviceName = text;
    setServices(updatedServices);
  };

  const handlePriceChange = (text, index) => {
    const updatedServices = [...services];
    updatedServices[index].price = text;
    setServices(updatedServices);
  };

  const handleSubmit = async () => {
    try {
      const business_id = id.id;

      await axios.post(`${API_BASE_URL}/service/createservice`, {
        business_id,
        services
      });

      Alert.alert('Services Submitted', 'Services created successfully');
      setServices([]);
      getData();
    } catch (error) {
      console.error('Error submitting services:', error.message);
      Alert.alert('Error', 'Failed to submit services. Please try again.');
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      const business_id = id.id;
      console.log('inside get data', business_id);
      const response = await axios.get(`${API_BASE_URL}/service/getservices`, {
        params: { business_id }
      });
      setData(response.data);
    } catch (error) {
      console.error('Error fetching data:', error.message);
      Alert.alert('Error', 'Failed to fetch services. Please try again.');
    }
  };

  const confirmDeleteService = (index) => {
    Alert.alert(
      'Confirm Delete',
      'Are you sure you want to delete this service?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => handleDeleteService(index),
        },
      ],
      { cancelable: false }
    );
  };

  const handleDeleteService = async (index) => {
    try {
      const serviceId = data[index]._id; // Assuming each service has a unique id
    console.log(serviceId);
      await axios.delete(`${API_BASE_URL}/service/deleteservice/${serviceId}`);
      getData();
      Alert.alert('Service Deleted', 'Service deleted successfully');
    } catch (error) {
      console.error('Error deleting service:', error.message);
      Alert.alert('Error', 'Failed to delete service. Please try again.');
    }
  };

  const handleEditService = (index) => {
    const serviceToEdit = data[index];
    setServices([{ serviceName: serviceToEdit.service, price: serviceToEdit.price }]);
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.tableHeader}>
        <Text style={styles.tableHeaderText}>Service</Text>
        <Text style={styles.tableHeaderText}>Price</Text>
      </View>
      {data.map((dat, index) => (
        <View key={index} style={styles.tableRow}>
          <Text style={styles.tableCell}>{dat.service}</Text>
          <Text style={styles.tableCell}>{dat.price}</Text>
          <View style={styles.tableCell}>
            <TouchableOpacity style={styles.deleteButton} onPress={() => confirmDeleteService(index)}>
              <Text style={styles.buttonText}>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
      ))}

      {services.map((service, index) => (
        <View key={index} style={styles.serviceContainer}>
          <TextInput
            style={[styles.input, styles.serviceNameInput]}
            placeholder="Enter service name"
            value={service.serviceName}
            onChangeText={(text) => handleServiceNameChange(text, index)}
          />
          <TextInput
            style={[styles.input, styles.priceInput]}
            placeholder="Enter price"
            value={service.price}
            onChangeText={(text) => handlePriceChange(text, index)}
            keyboardType="numeric"
          />
        </View>
      ))}
      <TouchableOpacity style={styles.addButton} onPress={handleAddService}>
        <Text style={styles.addButtonText}>+ Add Service</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.submitButton} onPress={handleSubmit}>
        <Text style={styles.buttonText}>Submit</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    paddingBottom: 10,
    marginBottom: 10,
  },
  tableHeaderText: {
    flex: 1,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  tableCell: {
    flex: 1,
    textAlign: 'center',
  },
  serviceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  input: {
    flex: 1,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  serviceNameInput: {
    marginRight: 10,
  },
  priceInput: {
    marginRight: 10,
  },
  addButton: {
    backgroundColor: '#2D9D1A',
    padding: 10,
    borderRadius: 25,
    alignItems: 'center',
    marginBottom: 20,
  },
  addButtonText: {
    color: '#FFF',
    fontSize: 12,
  },
  submitButton: {
    backgroundColor: '#2D9D1A',
    padding: 10,
    borderRadius: 25,
    alignItems: 'center',
  },
  buttonText: {
    color: '#FFF',
    fontSize: 12,
  },
  editButton: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    marginRight: 5,
  },
  deleteButton: {
    backgroundColor: 'red',
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
  },
});

export default MenuSer;
