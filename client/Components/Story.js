import { StyleSheet, Text, View, Image, ScrollView, Modal, TouchableOpacity, Dimensions, PanResponder } from 'react-native';
import React, { useState, useRef, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';

const Story = ({ stories }) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [currentImageIndex, setCurrentImageIndex] = useState(0);
    const [panResponder, setPanResponder] = useState(null);

    const modalScrollViewRef = useRef(null);
    const navigation = useNavigation();

    useEffect(() => {
        const panResponderInstance = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: handlePanResponderMove,
            onPanResponderRelease: () => {},
        });
        setPanResponder(panResponderInstance);
    }, []);

    const toggleModal = (index) => {
        setCurrentImageIndex(index);
        setModalVisible(!modalVisible);
    };

    const handleIconPress = () => {
        navigation.navigate('CreateStory');
    };

    const renderImages = () => {
        return stories.map((story, storyIndex) => (
            <View key={storyIndex} style={styles.story}>
                {story.image_paths.map((imageUri, imageIndex) => (
                    <TouchableOpacity key={imageIndex} onPress={() => toggleModal(imageIndex)}>
                        <Image
                            style={styles.image}
                            source={{ uri: `http://10.9.84.110:8080/${imageUri}` }}
                        />
                    </TouchableOpacity>
                ))}
            </View>
        ));
    };

    const handlePanResponderMove = (evt, gestureState) => {
        if (gestureState.dx < -50 && currentImageIndex < stories.length - 1) {
            setCurrentImageIndex(currentImageIndex + 1);
            modalScrollViewRef.current.scrollTo({
                x: currentImageIndex * Dimensions.get('window').width,
                animated: true,
            });
        } else if (gestureState.dx > 50 && currentImageIndex > 0) {
            setCurrentImageIndex(currentImageIndex - 1);
            modalScrollViewRef.current.scrollTo({
                x: currentImageIndex * Dimensions.get('window').width,
                animated: true,
            });
        }
    };

    const navigateLeft = () => {
        if (currentImageIndex > 0) {
            setCurrentImageIndex(currentImageIndex - 1);
            modalScrollViewRef.current.scrollTo({
                x: (currentImageIndex - 1) * Dimensions.get('window').width,
                animated: true,
            });
        }
    };

    const navigateRight = () => {
        if (currentImageIndex < stories.length - 1) {
            setCurrentImageIndex(currentImageIndex + 1);
            modalScrollViewRef.current.scrollTo({
                x: (currentImageIndex + 1) * Dimensions.get('window').width,
                animated: true,
            });
        }
    };

    return (
        <ScrollView horizontal contentContainerStyle={styles.scrollViewContent}>
            <View style={styles.storiesContainer}>
                <TouchableOpacity onPress={handleIconPress}>
                    <View style={styles.story}>
                        <Icon name="add-circle" size={50} color="#2D9D1A" style={{ marginLeft: 8, marginTop: 5 }} />
                        <Text style={{ marginLeft: 8 }}>Add </Text>
                        <Text style={{ marginLeft: 8 }}>Story</Text>
                    </View>
                </TouchableOpacity>
                {renderImages()}
            </View>
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <TouchableOpacity onPress={() => setModalVisible(false)} style={styles.closeButton}>
                        <Icon name="close-circle" size={35} color="#2D9D1A" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.arrowLeft} onPress={navigateLeft}>
                        <Icon name="chevron-back-circle" size={35} color="#2D9D1A" />
                    </TouchableOpacity>
                    <ScrollView
                        horizontal
                        pagingEnabled
                        ref={modalScrollViewRef}
                        {...(panResponder && panResponder.panHandlers)}
                    >
                        {stories[currentImageIndex]?.image_paths.map((imageUri, imageIndex) => (
                            <Image
                                key={imageIndex}
                                style={styles.fullScreenImage}
                                source={{ uri: `http://10.9.89.179:8080/${imageUri}` }}
                            />
                        ))}
                    </ScrollView>
                    <TouchableOpacity style={styles.arrowRight} onPress={navigateRight}>
                        <Icon name="chevron-forward-circle" size={35} color="#2D9D1A" />
                    </TouchableOpacity>
                </View>
            </Modal>
        </ScrollView>
    );
};

export default Story;

const styles = StyleSheet.create({
    scrollViewContent: {
        flexDirection: 'row',
        flexGrow: 1,
    },
    storiesContainer: {
        display: 'flex',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        marginTop: 10,
        backgroundColor: 'white',
    },
    story: {
        borderWidth: 3,
        borderColor: '#2D9D1A'
        ,
        height: 120,
        width: 90,
        marginLeft: 3,
        borderRadius: 10,
        marginBottom: 10,
        marginRight: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        height: 120,
        width: 90,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#2D9D1A',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    closeButton: {
        position: 'absolute',
        top: 20,
        right: 20,
        zIndex: 1,
    },
    fullScreenImage: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    arrowLeft: {
        position: 'absolute',
        top: '50%',
        left: 20,
        zIndex: 1,
    },
    arrowRight: {
        position: 'absolute',
        top: '50%',
        right: 20,
        zIndex: 1,
    },
});


