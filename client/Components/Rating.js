import React, { useEffect, useState } from 'react';
import { View, FlatList, Text, StyleSheet, TouchableOpacity ,TextInput, Button} from 'react-native';
import Navbar from '../Components/Navbar';
import { useRoute } from '@react-navigation/native';
import axios from 'axios';
import StarRating from './StarRating';

// const API_BASE_URL = 'http://10.9.89.179:8080/api/v1';
const API_BASE_URL = 'http://10.9.84.110:8080/api/v1';




const NotificationItem = ({ title, description }) => (
    <TouchableOpacity style={styles.notificationItem}>
        <Text style={styles.notificationTitle}>{title}</Text>
        <Text style={styles.notificationDescription}>{description}</Text>
    </TouchableOpacity>
);

const Rating = () => {
    const route = useRoute();
    const id = route.params?.data || '';
    const businessId = id.id;

    const [ratings, setRatings] = useState([]);
    const [ratingText, setRatingText] = useState('');
    const [ratingScore, setRatingScore] = useState('');
    useEffect(()=>{
        fetchRatings()
    },[businessId])
    const fetchRatings = async () => {
        try {
            const response = await axios.get(`${API_BASE_URL}/rating/getrating/${businessId}`);
            setRatings(response.data.ratings);
        } catch (error) {
            console.error('Error fetching ratings:', error);
        }
    };
    const renderRating = ({ item }) => (
        <View style={styles.ratingRow}>
            <Text style={styles.ratingText}>{item.content}</Text>
            <StarRating rating={parseInt(item.ratingScore)} />
        </View>
    );
    const submitRating = async () => {
        
        if (ratingText && ratingScore) {
            try {
                const datatosend ={
                    business_id: businessId,
                    ratingText: ratingText,
                    ratingScore: ratingScore,
                    business : businessId,
                    user: null,
                }
                await axios.post(`${API_BASE_URL}/rating/addrating`, datatosend);
                setRatingText('');
                setRatingScore('');
                fetchRatings();
            } catch (error) {
                console.error('Error submitting rating:', error);
            }
        }
    };
    return (
        <View style={styles.maincontainer}>
            {ratings.length === 0 ? (
                        <Text style={styles.noRatingsText}>No ratings yet.</Text>
                    ) : (
                        <FlatList
                            data={ratings}
                            renderItem={renderRating}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    )}
                    <View style={styles.ratingForm}>
                        <TextInput
                            style={styles.ratingInput}
                            placeholder="Rating Text"
                            value={ratingText}
                            onChangeText={setRatingText}
                        />
                        <TextInput
                            style={styles.ratingInput}
                            placeholder="Rating Score (1-5)"
                            value={ratingScore}
                            onChangeText={setRatingScore}
                            keyboardType="numeric"
                        />
                        <Button title="Submit" onPress={submitRating} />
                    </View>
            <View style={styles.navbarContainer}>
                <Navbar />
            </View>
        </View>
    );
};

export default Rating ;

const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
        padding: 20,
        backgroundColor: '#fff',
    },
    notificationList: {
        flex: 1,
        paddingHorizontal: 20,
    },
    notificationListContent: {
        paddingVertical: 20,
    },
    notificationItem: {
        backgroundColor: '#fff',
        padding: 15,
        marginBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ddd',
    },
    notificationTitle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    notificationDescription: {
        fontSize: 14,
        marginTop: 5,
    },
    navbarContainer: {
        height: '10%',
        justifyContent: 'flex-end',
    },
    ratingRow: {
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    ratingScore: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    ratingText: {
        fontSize: 16,
        color: '#888',
    },
    noRatingsText: {
        fontSize: 16,
        color: 'gray',
        textAlign: 'center',
        marginTop: 20,
    },
    ratingForm: {
        marginTop: 20,
    },
    ratingFormTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center',
    },
    ratingInput: {
        borderWidth: 1,
        borderColor: '#ccc',
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
});
