import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Navigation from './Navigation';
import { NavigationContainer } from '@react-navigation/native';
import RegisterPage from './Screens/RegisterPage';
import LoginBusiness from './Screens/LoginBusiness';
import { createStackNavigator } from '@react-navigation/stack';
import SignupUser from './Screens/SignupUser';
import SignupBusiness from './Screens/SignupBusiness';
import HomePage from './Screens/HomePage';
import UserProfile from './Screens/UserProfile';
import Useredit from './Screens/Useredit';
import UserNotification from './Screens/UserNotification';
import BusinessHome from './Screens/BusinessHome';
import BusinessProfile from './Screens/BusinessProfile';
import Manage from './Screens/Manage';
import Rating from './Components/Rating';
import MenuSer from './Components/MenuSer';
import CreatePost from './Screens/CreatePost';
import BusinessNotification from './Screens/BusinessNotification';
import CreateStory from './Screens/CreateStory';
import UserStory from './Components/UserStory';
import ServiceView from './Screens/ServiceView';
export default function App() {
  const Stack = createStackNavigator();
  return(
    // Navigation()
    //<RegisterPage/>
    //<LoginBusiness/>


    <NavigationContainer>
    <Stack.Navigator options={{ headerShown: false }} initialRouteName="Register">
      <Stack.Screen name="Register" component={RegisterPage} options={{ headerShown: false }} />
      <Stack.Screen name="Login" component={LoginBusiness} options={{ headerShown: false }} />
      <Stack.Screen name="UserRegister" component={SignupUser} options={{ headerShown: false }} />
      <Stack.Screen name="BusinessRegister" component={SignupBusiness} options={{ headerShown: false }} />
      <Stack.Screen name="Home" component={HomePage} options={{ headerShown: false }} />
      <Stack.Screen name="UserProfile" component={UserProfile} options={{ headerShown: false }} />
      <Stack.Screen name="Useredit" component={Useredit} options={{ headerShown: false }} />
      <Stack.Screen name="UserNotification" component={UserNotification} options={{ headerShown: false }} />
      <Stack.Screen name="BusinessHome" component={BusinessHome} options={{ headerShown: false }} />
      <Stack.Screen name="BusinessProfile" component={BusinessProfile} options={{ headerShown: false }} />
      <Stack.Screen name="Manage" component={Manage} options={{ headerShown: false }} />
      <Stack.Screen name="Rating" component={Rating} options={{ headerShown: false }} />
      <Stack.Screen name="MenuSer" component={MenuSer} options={{ headerShown: false }} />
      <Stack.Screen name="CreatePost" component={CreatePost} options={{ headerShown: false }} />
      <Stack.Screen name="BusinessNotification" component={BusinessNotification} options={{ headerShown: false }} />
      <Stack.Screen name="CreateStory" component={CreateStory} options={{ headerShown: false }} />
      <Stack.Screen name="UserStory" component={UserStory} options={{ headerShown: false }} />
      <Stack.Screen name="ServiceView" component={ServiceView} options={{ headerShown: false }} />

    </Stack.Navigator>
  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
