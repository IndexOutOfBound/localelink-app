import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomePage from "./Screens/HomePage";

const Stack=createStackNavigator();

const Navigation =()=> {
   return(
        <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown:false}}>
            <Stack.Screen 
                name="Home"
                component={HomePage}
            />
        </Stack.Navigator>
    </NavigationContainer>
   );
}

export default Navigation;