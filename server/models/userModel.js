import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import JWT from 'jsonwebtoken';

const userSchema = new mongoose.Schema({

    email:{
        type:String,
        required:[true, 'email is required'],
        unique:[true,'email is already taken']
    },
    name:{
        type:String,
        required:[true, 'name is required'],
    },

    password:{
        type:String,
        required:[true, 'email is required'],
    },
    profilePic: {
        public_id: {
          type: String,
        },
        url: {
          type: String,
        },
      },
},{timestamps:true});


//functions

//hash functions

userSchema.pre('save', async function(next) {
    if (!this.isModified('password')) {
        return next();
    }

    try {
        // Hash the password
        this.password = await bcrypt.hash(this.password, 10);
        next();
    } catch (error) {
        next(error);
    }
});


//compare functions

userSchema.methods.comparePassword = async function(plainPassword){
    return await bcrypt.compare(plainPassword, this.password)
}

// JWT token
userSchema.methods.generateToken = function(){
    return JWT.sign({_id:this._id}, process.env.JWT_secret,{expiresIn:"7d"});
}





export const userModel = mongoose.model('Users',userSchema);

export default userModel;