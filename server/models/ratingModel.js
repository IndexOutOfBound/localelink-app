import mongoose from 'mongoose';

const ratingSchema = new mongoose.Schema({
    business_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business',
        required: true,
    },
    business:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business',
        required:false,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    content: {
        type: String,
        required: true,
    },
    ratingScore: {
        type: Number,
        required: true,
        min: 1,
        max: 5,
    },
}, { timestamps: true });

export default mongoose.model('Rating', ratingSchema);
