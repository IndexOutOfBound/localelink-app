import mongoose from 'mongoose';

const StorySchema = new mongoose.Schema({
    image_paths: [{
        type: String,
        required: true,
    }],
    business: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business',
        required: true,
    }
}, { timestamps: true });

const Story = mongoose.model('Story', StorySchema);
export default Story;
