import mongoose from 'mongoose';

const postSchema = new mongoose.Schema({
    image_paths: [{
        type: String,
        required: true,
    }],
    image_captions: {
        type: String,
        required: true,
    },
    business: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business',
        required: true,
    }
}, { timestamps: true });

const Post = mongoose.model('Post', postSchema);
export default Post;
