import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import JWT from 'jsonwebtoken';

const businessSchema = new mongoose.Schema({

    email:{
        type:String,
        required:[true, 'email is required'],
        unique:[true,'email is already taken']
    },
    businessname:{
        type:String,
        required:[true, 'Business Name is required'],
    },
    phonenumber:{
        type:String,
        required:[true, 'Phonenumber is required'],
    },
    location:{
        type:String,
        required:[true, 'Location is required'],
    },
    category:{
        type:String,
        required:[true, 'Category is required'],
    },
    aboutbusiness:{
        type:String,
        required:[true, 'About Business is required'],
    },
    
    password:{
        type:String,
        required:[true, 'email is required'],
    },
    profilePic: {
        public_id: {
          type: String,
        },
        url: {
          type: String,
        },
      },
},{timestamps:true});


//functions

//hash functions

businessSchema.pre('save', async function(next) {
    if (!this.isModified('password')) {
        return next();
    }

    try {
        // Hash the password
        this.password = await bcrypt.hash(this.password, 10);
        next();
    } catch (error) {
        next(error);
    }
});


//compare functions

businessSchema.methods.comparePassword = async function(plainPassword){
    return await bcrypt.compare(plainPassword, this.password)
}

// JWT token
businessSchema.methods.generateToken = function(){
    return JWT.sign({_id:this._id}, process.env.JWT_secret,{expiresIn:"7d"});
}





export const businessModel = mongoose.model('Business',businessSchema);

export default businessModel;