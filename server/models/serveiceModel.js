import mongoose from 'mongoose';


// Define the schema for the service model
const serviceSchema = new mongoose.Schema({
    business_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Business', // Reference to the Business model if needed
        required: true
    },
    service: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true
    }
});

// Create the model based on the schema
const Service = mongoose.model('Service', serviceSchema);

// Export the model
export default Service;
