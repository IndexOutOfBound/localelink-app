import express from 'express';
import { createService, getAllServices, updateService, deleteService, getdetailsforuser } from '../controllers/serviceController.js';

const router = express.Router();

router.post('/createservice', createService);
router.get('/getservices', getAllServices)
router.put('/updateservice/:serviceId', updateService);
router.delete('/deleteservice/:serviceId', deleteService);
router.get('/getdetailsforuser/:businessId',getdetailsforuser)

export default router;