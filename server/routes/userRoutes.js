import express from 'express';
import { loginController, registerController,userprofileController,logoutController,updateprofileController,updatepasswordController,updateProfilePicController } from '../controllers/userController.js';
import { isAuth } from '../middleware/authMW.js';
import { singleUpload } from "../middleware/multer.js";

//router object

const router = express.Router();

//routes

//register
router.post('/register',registerController)

//login
router.post('/login',loginController)


//userprofile
router.get('/profile',isAuth, userprofileController)


//logout
router.get('/logout',isAuth,logoutController)


//profile update
router.put('/profileUpdate',isAuth,updateprofileController)


//update password

router.put('/updatePassword',isAuth,updatepasswordController)

// update profile pic
router.put("/update-picture", isAuth, singleUpload, updateProfilePicController);

export default router;
