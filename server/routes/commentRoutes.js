import express from 'express';
import { createComment, getAllComments } from '../controllers/commentController.js';

const router= express.Router();


router.post('/create/:post_id',createComment)

router.get('/getallcomments',getAllComments)

export default router;