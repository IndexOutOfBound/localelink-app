// postRoutes.js
import express from 'express';
import { likePost, unlikePost, getLikeCount   } from '../controllers/likeController.js';

const router = express.Router();

router.post('/like', likePost);
router.post('/unlike', unlikePost);
router.get('/count/:postId', getLikeCount);
export default router;
