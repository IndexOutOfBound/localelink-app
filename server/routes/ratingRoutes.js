import express from 'express';
import { addrating, getrating } from '../controllers/ratingController.js';

const router= express.Router();


router.post('/addrating',addrating)

router.get('/getrating/:businessId',getrating)

export default router;