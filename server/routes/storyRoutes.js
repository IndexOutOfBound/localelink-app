// postRoutes.js
import express from 'express';
import upload from './fileUpload.js';
//import { createPost, getImage, getPosts } from '../controllers/postController.js';
import {createStory , getStory } from '../controllers/storyController.js';

const router = express.Router();

router.post('/create', upload.array('images', 10), createStory);
router.get('/getstory', getStory)
// router.get('/images/:imageName', getImage);

export default router;
