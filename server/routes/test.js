import express from 'express';
import { testController } from '../controllers/testController.js';

//router object

const router = express.Router();


//routes
router.get('/testok', testController)


export default router