import express from 'express';
import {addBusiness, updateBusiness, deleteBusiness,businessLogin, getuserdata, updateuserdata, getBusinessData} from '../controllers/businessController.js';
import { isAuth } from '../middleware/authMW.js';


//router object

const router = express.Router();

//routes

//register
router.post('/addusers', addBusiness)

router.post('/login', businessLogin)

router.get('/getuserdata/:userId', getuserdata)

router.put('/updateuserdata/:userId', updateuserdata)

router.get('/getbusinessdata/:businessId', getBusinessData)



export default router;
