// postRoutes.js
import express from 'express';
import upload from './fileUpload.js';
import { createPost, getImage, getPosts, getPostsForBusiness } from '../controllers/postController.js';

const router = express.Router();

router.post('/create', upload.array('images', 10), createPost);
router.get('/getposts', getPosts)
router.get('/images/:imageName', getImage);
router.get('/businessposts/:businessId', getPostsForBusiness)

export default router;
