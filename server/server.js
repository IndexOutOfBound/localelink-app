import express from 'express';
import colors from 'colors';
import morgan from 'morgan';
import cors from 'cors';
import dotenv from 'dotenv';
import cookieParser from 'cookie-parser';
import cloudinary from "cloudinary";
import path from 'path';
import { fileURLToPath } from 'url';

const app = express();

// Create __dirname equivalent for ES modules
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Serve static files from the "uploads" directory
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

// routes
import test from './routes/test.js';
import userRoutes from './routes/userRoutes.js';
import businessRoutes from './routes/bussinessRoutes.js'
import postRoutes from './routes/postRoutes.js';
import storyRoutes from './routes/storyRoutes.js';
import likeRoutes from './routes/likeRoutes.js';
import serviceRoutes from './routes/serviceRoutes.js';
import commentRoutes from './routes/commentRoutes.js';
import ratingRoutes from './routes/ratingRoutes.js';
//routes import

import connectDB from './config/db.js';

//config dotenv config should always be first
dotenv.config();


//db connection

connectDB();




//cloudinary Config
cloudinary.v2.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_SECRET,
  });
  
//rest object

//middleware

app.use(morgan("dev"));
app.use(express.json());
app.use(cors());
app.use(cookieParser());


//route
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use('/api/v1/',test);
app.use('/api/v1/user',userRoutes);
app.use('/api/v1/bussines',businessRoutes);
app.use('/api/v1/posts',postRoutes);
app.use('/api/v1/story',storyRoutes);
app.use('/api/v1/like',likeRoutes);
app.use('/api/v1/service', serviceRoutes);
app.use('/api/v1/comment', commentRoutes);
app.use('/api/v1/rating', ratingRoutes);

app.get("/",(req, res) => {
    return res.status(200).send("<h1>Welcomejn</h1>")
});

//port
const port=process.env.PORT || 8080; // if port is not found in proess it will use default 8080

//listen
app.listen(port,()=>{
    
    console.log(`server running on port ${process.env.PORT} on ${process.env.NODE_ENV} mode`);
});
