import mongoose from "mongoose";
import colors from "colors";

const connectDB= async()=>{
    try{
        await mongoose.connect(process.env.MONGO_URL)
        console.log(`Mongo connection ${mongoose.connection.host}`)
    }catch(e){
        console.log(`Mongo error ${e}`.bgRed.white)
    }
}

export default connectDB;