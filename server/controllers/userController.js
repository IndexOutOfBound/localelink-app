import userModel from '../models/userModel.js'; 
import businessModel from '../models/bussinesModel.js';
import cloudinary from "cloudinary";
import { getDataUri } from "../utils/feature.js";  

export const registerController= async(req,res) =>{

    try{
        const {email,name, password}=req.body;

        if(!email || !password){
            return res.status(500).send({
                sucess: false,
                message:"please give all the fields"
            })
        }
        //check existing user
        const existingUser= await userModel.findOne({email})
        const existingUser1 = await businessModel.findOne({email})

        //validation
        if (existingUser || existingUser1) {
            return res.status(500).send({
                sucess: false,
                message:"Email already taken"
            })
        }

        const user = await userModel.create({email,name,password});

        res.status(201).send({
            sucess: true,
            message:"success Register, You can Login",
        });
    }catch(e){
        console.log(e);
        res.status(500).send({
            sucess: false,
            mesage:"Server Error, try again later",
            e
        })
    }
};


//login

export const loginController = async (req, res) => {

    try{
        const {email,password}=req.body;
        if(!email || !password){
            return res.status(500).send({
                sucess: false,
                message:"please give all the fields"
            })
        }
        //check existing user
        const user= await userModel.findOne({email});

        //validation
        if (!user){
            return res.status(500).send({
                sucess: false,
                message:"Email not found"
            })
        }

        //check password
        const isMatch=await user.comparePassword(password);

        //validation
        if(!isMatch){
            return res.status(500).send({
                sucess: false,
                message:"Invalid credentials"
            })
        }

        //token
        const token = user.generateToken();
        res.status(200).cookie("token",token,{
            expires: new Date(Date.now() + 10 *24 *60 *60 *1000),
            secure:process.env.NODE_ENV === "development" ? true : false,
            httpOnly:process.env.NODE_ENV === "development" ? true : false,
            sameSite:process.env.NODE_ENV === "development" ? true : false
            
        }).send({
            sucess: true,
            message:"loged in",
            user,
            token
        }
            
        );

    }catch(e){
        console.log(e);
        res.status(500).send({
            sucess: false,
            mesage:"error in login Api",
            e
        })
    }
}

//get userProfile

export const userprofileController= async (req,res)=>{
    try{
        const user = await userModel.findById(req.user._id);
        res.status(200).send({
            sucess: true,
            message: "user profile fetch",
           user
        })
    }catch(e){
        console.log(e);
        res.status(500).send({
            sucess: false,
            mesage:"error in profile Api",
            e
        })
    }
};


//Logout
export const logoutController =async(req, res)=>{
 try{
    res.status(200).cookie("token","",{
        expires: new Date(Date.now()),
        secure:process.env.NODE_ENV === "development" ? true : false,
        httpOnly:process.env.NODE_ENV === "development" ? true : false,
        sameSite:process.env.NODE_ENV === "development" ? true : false
        
    }).send({
        sucess:true,
        message:"logout Sucessfully"
    })


 }catch(e){
    res.status(500).send({
        sucess: false,
        message: "User not logged Out",
        e
    }
    )
 }
}; 

//profile update
export const updateprofileController= async(req,res) => {
    try{
        const user = await userModel.findById(req.user._id);
        const {email,password} =req.body

        //validation + update

        if(email) user.email = email
        if(password) user.password = password

        //saveuser
        await user.save()
        res.status(200).send({
            sucess: true,
            message: 'Your profile has been updated'
        })

    }catch(e){
        res.status(500).send({
            sucess: false,
            message: "User not updated",
            e
        })
    }
}

//update password
export const updatepasswordController= async(req,res) => {
    try{
        const user = await userModel.findById(req.user._id);
        const {oldPassword,newPassword} =req.body

        //validation + update

        if(!oldPassword || !newPassword){
            return res.status(500).send({
                sucess: false,
                message: 'enter old or new password',
            })
        }
        //old password check
        const isMatch= await user.comparePassword(oldPassword)

        if(!isMatch){
          return  res.status(500).send({
            sucess: false,
            message: 'Invalid old Password',
          }
            )
        }

        user.password=newPassword;
        await user.save();
        res.status(200).send({
            sucess: true,
            message: 'Password updated successfully'
        })

        //saveuser
        await user.save()
        res.status(200).send({
            sucess: true,
            message: 'Your profile has been updated'
        })

    }catch(e){
        res.status(500).send({
            sucess: false,
            message: "error in password update",
            e
        })
    }
}


/// Update user profile photo
export const updateProfilePicController = async (req, res) => {
    try {
      const user = await userModel.findById(req.user._id);
      // file get from client photo
      const file = getDataUri(req.file);
      // delete prev image
     // await cloudinary.v2.uploader.destroy(user.profilePic.public_id);
      // update
      const cdb = await cloudinary.v2.uploader.upload(file.content);
      user.profilePic = {
        public_id: cdb.public_id,
        url: cdb.secure_url,
      };
      // save func
      await user.save();
  
      res.status(200).send({
        success: true,
        message: "profile picture updated",
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({
        success: false,
        message: "Error In update profile pic API",
        error,
      });
    }
  };