import businessModel from '../models/bussinesModel.js'; 
import userModel from '../models/userModel.js';
import cloudinary from "cloudinary";
import { getDataUri } from "../utils/feature.js";  

export const addBusiness = async(req,res)=>{
    try{
        const existingUser = await businessModel.findOne({email:req.body.email});
        const existingUser1 = await userModel.findOne({email:req.body.email});

        if (existingUser || existingUser1) {
            return res.status(500).send({
                success:false,
                message:"The user with this email already exists"
            })
        }
        const {email, name, phonenumber, location, category,aboutbusiness,password} =req.body;
        const business = await businessModel.create({
            email:email,
            businessname:name,
            phonenumber:phonenumber,
            location:location,
            category:category,
            aboutbusiness:aboutbusiness,
            password:password
        });

        return res.status(201).send({
            success:true,
            message:"Registration successful"
        })
    }catch(e){
        console.log(e);
    }
}

export const businessLogin = async (req, res) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(400).send({
                success: false,
                message: "Please provide both email and password"
            });
        }

        // Check for the email in businessModel
        let user = await businessModel.findOne({ email });
        let userType = "business";

        // If not found in businessModel, check in userModel
        if (!user) {
            user = await userModel.findOne({ email });
            userType = "user";
        }

        // If user is still not found
        if (!user) {
            return res.status(404).send({
                success: false,
                message: "Email not found"
            });
        }

        // Validate the password
        const isMatch = await user.comparePassword(password);

        // If password does not match
        if (!isMatch) {
            return res.status(400).send({
                success: false,
                message: "Invalid credentials"
            });
        }

        // Send response with user type
        res.status(200).send({
            success: true,
            message: "Logged in successfully",
            user,
            userType // Send the type of user
        });

    } catch (e) {
        console.log(e);
        res.status(500).send({
            success: false,
            message: "Error in login API",
            error: e
        });
    }
};

export const getBusinessData = async(req,res)=>{
    try {
        const businessId = req.params.businessId;
        const business = await businessModel.findById(businessId);
        if (!business) {
          return res.status(404).json({ message: 'User not found' });
        }
        res.status(200).json(business);
      } catch (error) {
        console.error('Error fetching user data:', error);
        res.status(500).json({ message: 'Internal server error' });
      }
}

export const updateBusiness = async(req,res)=>{

}

export const deleteBusiness = async(req,res)=>{

}


export const getuserdata =async(req,res)=>{
    try {
        const userId = req.params.userId;
        const user = await userModel.findById(userId);
        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }
        res.status(200).json(user);
      } catch (error) {
        console.error('Error fetching user data:', error);
        res.status(500).json({ message: 'Internal server error' });
      }
}


export const updateuserdata = async (req,res)=>{
    try {
        const userId = req.params.userId;
        const { name, email } = req.body;
    
        // Update user data
        const updatedUser = await userModel.findByIdAndUpdate(
          userId,
          { name, email },
          { new: true } 
        );
    
        if (!updatedUser) {
          return res.status(404).json({ message: 'User not found' });
        }
    
        res.status(200).json(updatedUser);
      } catch (error) {
        console.error('Error updating user profile:', error);
        res.status(500).json({ message: 'Internal server error' });
      }
}