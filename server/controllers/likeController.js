
import likeModel from '../models/likesModel.js';
import postModel from '../models/postModel.js';
import userModel from '../models/userModel.js';
import businessModel from '../models/bussinesModel.js';

// Like a post
export const likePost = async (req, res) => {
    try {
        const { postId, userId, businessId } = req.body;

        // Validate that either userId or businessId is provided
        if (!userId && !businessId) {
            return res.status(400).json({ message: 'Either userId or businessId must be provided' });
        }

        // Check if the user or business has already liked the post
        const existingLike = await likeModel.findOne({ post: postId, $or: [{ user: userId }, { business: businessId }] });

        if (existingLike) {
            return res.status(400).json({ message: 'Post already liked by this user or business' });
        }

        // Create a new like
        const newLike = new likeModel({ post: postId, user: userId || null, business: businessId || null });
        await newLike.save();

        // Optionally update the post like count
        await postModel.findByIdAndUpdate(postId, { $inc: { likeCount: 1 } });

        res.status(201).json({ message: 'Post liked successfully', like: newLike });
    } catch (error) {
        console.log(error.message);
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};

// Unlike a post
export const unlikePost = async (req, res) => {
    try {
        const { postId, userId, businessId } = req.body;

        // Validate that either userId or businessId is provided
        if (!userId && !businessId) {
            return res.status(400).json({ message: 'Either userId or businessId must be provided' });
        }

        // Find the like entry to delete
        const existingLike = await likeModel.findOneAndDelete({ post: postId, $or: [{ user: userId }, { business: businessId }] });

        if (!existingLike) {
            return res.status(400).json({ message: 'Like not found for this user or business' });
        }

        // Optionally update the post like count
        await postModel.findByIdAndUpdate(postId, { $inc: { likeCount: -1 } });

        res.status(200).json({ message: 'Post unliked successfully' });
    } catch (error) {
        console.log(error.message);
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};


// Get like count for a post
export const getLikeCount = async (req, res) => {
    try {
        const { postId } = req.params;

        // Count the number of likes for the post
        const likeCount = await likeModel.countDocuments({ post: postId });

        res.status(200).json({ likeCount });
    } catch (error) {
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};
