// serviceController.js

import serviceModel from '../models/serveiceModel.js';
import businessModel from '../models/bussinesModel.js'; 

// Create a new service
export const createService = async (req, res) => {
    try {
        const { business_id, services } = req.body;

        // Create an array to store new service instances
        const newServices = [];

        // Loop through each service in the request body and create a new service instance
        for (const service of services) {
            console.log(service);
            const { serviceName, price} = service;

            const newService = new serviceModel({
                business_id,
                service: serviceName,
                price
            });

            newServices.push(await newService.save());
        }

        res.status(201).json(newServices);
    } catch (error) {
        console.log(error.message)
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};

// Get all services
export const getAllServices = async (req, res) => {
    try {
        const { business_id } = req.query; // Use req.query to get the business_id
        console.log(business_id);
        if (!business_id) {
            return res.status(400).json({ message: 'business_id is required' });
        }

        // Find all services for the specified business_id
        const services = await serviceModel.find({ business_id });

        res.status(200).json(services);
    } catch (error) {
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};



// Update a service by ID
export const updateService = async (req, res) => {
    try {
        const { id } = req.params;
        const { business_id, service, price } = req.body;

        // Find the service by ID and update its attributes
        const updatedService = await serviceModel.findByIdAndUpdate(id, { business_id, service, price }, { new: true });

        if (!updatedService) {
            return res.status(404).json({ message: 'Service not found' });
        }

        res.status(200).json(updatedService);
    } catch (error) {
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};

// Delete a service by ID
export const deleteService = async (req, res) => {
    try {
        const { serviceId } = req.params;
        console.log(req.params)
        console.log(serviceId)
        // Find the service by ID and delete it
        const deletedService = await serviceModel.findByIdAndDelete(serviceId);

        if (!deletedService) {
            return res.status(404).json({ message: 'Service not found' });
        }

        res.status(200).json({ message: 'Service deleted successfully' });
    } catch (error) {
        console.log(error.message);
        res.status(500).json({ message: 'Server error', error: error.message });
    }
};


export const getdetailsforuser = async (req,res)=>{
    try {
        const {businessId} = req.params;
        console.log(businessId);
        const business = await businessModel.findById(businessId);
        console.log(business)
        const services = await serviceModel.find({ business_id: businessId });

        if (!business) {
            return res.status(404).json({ message: 'Business not found' });
        }

        res.json({
            business,
            services,
        });
    } catch (error) {
        res.status(500).json({ message: 'Server error', error });
    }
}