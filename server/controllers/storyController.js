import businessModel from "../models/bussinesModel.js";
import postModel from "../models/postModel.js";
import moment from "moment";
import Story from "../models/storyModel.js";

export const createStory = async (req, res) => {
    try {
        const {businessId } = req.body;
        const image_paths = req.files.map(file => file.path);

        console.log(image_paths)
        console.log(req.body)
        // Validate that the business exists
        const business = await businessModel.findById(businessId);
        if (!business) {
            return res.status(404).json({ message: 'Business not found' });
        }

        // Create a new post
        const newStory = new Story({
            image_paths,
            business: business._id
        });

        // Save the post to the database
        const savedStory = await newStory.save();

        res.status(201).send({
            success: true,
            message: "Posted Successfully"
        })
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Serve the image files
// export const getImage = async (req, res) => {
//     try {
//         const { imageName } = req.params;
//         const imagePath = path.resolve(`uploads/${imageName}`);
//         res.sendFile(imagePath);
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// };


export const getStory = async (req, res) => {
    try {
        const storys = await Story.find().populate('business', 'businessname');

        console.log(storys);
        const formattedStory = storys.map(story => {
            return {
                ...story._doc,
                createdAt: moment(story.createdAt).format('DD/MM/YYYY')
            };
        });

        res.status(200).send({
            success: true,
            message: "Fetched successfully",
            storys: formattedStory
        });
    } catch (error) {
        res.status(500).json({ message: error.message });
        console.log(error.message);
    }
}