import commentModel from "../models/commentModel.js";

export const createComment = async(req,res)=>{
    try{
        const {post_id} = req.params;
        const {business_id, user_id, content} = req.body;

        if(!business_id && !user_id){
            return res.status(400).json({message: "Missing data: Business ID or User ID"});
        }

        if(!post_id){
            return res.status(404).json({message: "Missing post ID"});
        }

        const newComment =new commentModel({
            post: post_id,
            user: user_id || null,
            business: business_id || null,
            content: content

        })

        await newComment.save();

        res.status(200).json({message: 'Comment added successfully' , data: newComment});
    }catch(e){
        console.log(e.message);
        res.status(500).send({message: e.message})
    }
}

export const getAllComments = async(req,res)=>{
    try{   
        const {post_id} = req.query;
        console.log(post_id);
        if(!post_id){
            return res.status(400).json({message: "Post ID is required"});
        }
        const result = await commentModel.find({post:post_id})
        res.status(200).json(result);
        console.log(result)
    }catch(e){
        console.log(e.message);
        res.status(500).send({message: e.message})
    }
}