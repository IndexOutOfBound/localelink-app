
import Rating from '../models/ratingModel.js';



// Add Review
export const addrating = async (req, res) => {
    const { business_id, ratingText, ratingScore, business,user } = req.body;
    console.log(req.body);
    try {
        const rating = new Rating({
            business_id: business_id,
            business:business,
            user:user,
            content: ratingText,
            ratingScore:ratingScore,
        });

        await rating.save();
        res.status(201).json({ message: 'Rating added successfully', rating });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Server error', error });
    }
};

// Get Reviews for Business
export const getrating = async (req, res) => {
    const { businessId } = req.params;

    try {
        const ratings = await Rating.find({ business_id: businessId });
        res.status(200).json({ ratings });
    } catch (error) {
        res.status(500).json({ message: 'Server error', error });
    }
};

