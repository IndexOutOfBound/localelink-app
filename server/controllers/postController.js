import businessModel from "../models/bussinesModel.js";
import postModel from "../models/postModel.js";
import likeModel from "../models/likesModel.js";  // Import the Like model
import moment from "moment";

export const createPost = async (req, res) => {
    try {
        const { image_captions, businessId } = req.body;
        const image_paths = req.files.map(file => file.path);

        console.log(image_paths);
        // Validate that the business exists
        const business = await businessModel.findById(businessId);
        if (!business) {
            return res.status(404).json({ message: 'Business not found' });
        }

        // Create a new post
        const newPost = new postModel({
            image_paths,
            image_captions,
            business: business._id
        });

        // Save the post to the database
        const savedPost = await newPost.save();

        res.status(201).send({
            success: true,
            message: "Posted Successfully"
        });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Serve the image files
export const getImage = async (req, res) => {
    try {
        const { imageName } = req.params;
        const imagePath = path.resolve(`uploads/${imageName}`);
        res.sendFile(imagePath);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const getPosts = async (req, res) => {
    try {
        const { userId, businessId } = req.query;  // Assuming you send userId or businessId from frontend

        const posts = await postModel.find().populate('business', 'businessname');

        // Prepare formatted posts with like count and liked status
        const formattedPosts = await Promise.all(posts.map(async post => {
            const likeCount = await likeModel.countDocuments({ post: post._id });

            // Determine if the post is liked by the current user or business
            const isLiked = await likeModel.findOne({
                post: post._id,
                $or: [
                    { user: userId },
                    { business: businessId }
                ]
            });

            return {
                ...post._doc,
                createdAt: moment(post.createdAt).format('DD/MM/YYYY'),
                likeCount,
                isLiked: !!isLiked  // Convert to boolean
            };
        }));

       
        res.status(200).send({
            success: true,
            message: "Fetched successfully",
            posts: formattedPosts
        });
    } catch (error) {
        res.status(500).json({ message: error.message });
        console.log(error.message);
    }
};


export const getPostsForBusiness = async(req,res)=>{
    try {
        const { businessId } = req.params;
        
        // Fetch posts for the given businessId and select only the first image and the post ID
        const posts = await postModel.find({ business: businessId }).select('image_paths _id');
    
        // Extract the first image from each post
        const firstImages = posts.map(post => ({
          postId: post._id,
          image: post.image_paths[0]
        }));
    
        res.status(200).json(firstImages);
      } catch (error) {
        console.error('Error fetching posts:', error);
        res.status(500).json({ success: false, message: 'Internal server error.' });
      }
}